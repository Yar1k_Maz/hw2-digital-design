const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const browserSync = require("browser-sync").create();
const htmlMin = require("gulp-htmlmin");
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
 

const html = () => {
  return gulp
    .src("./src/*.html")
    .pipe(htmlMin({ collapseWhitespace: true }))
    .pipe(gulp.dest("./dist"));
};


const scss = () => {
  return gulp
    .src("./src/style/**/*.{css,scss}")
    .pipe(sass.sync().on("error", sass.logError))
    .pipe(concat("style.css"))
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest("./dist/style/"));
};
const img = () => {
  return gulp.src("./src/img/*").pipe(gulp.dest("./dist/img/"));
};

const server = () => {
  browserSync.init({ server: { baseDir: "./dist" } });
};

const watcher = () => {
  gulp.watch("./src/index.html", html).on("all", browserSync.reload);
  gulp
    .watch("./src/style/**/*.{scss,sass,css}", scss)
    .on("all", browserSync.reload);
};

gulp.task("style", scss);
gulp.task("img", img);
gulp.task("htmlMin", html);
gulp.task("dev", gulp.parallel(html, scss, img, server, watcher));
